<?php

namespace Drupal\duet_date_picker\Element;

use Drupal\webform\Element\WebformMultiple;

use Drupal\Core\Render\Element\FormElement;

/**
 * Handle the creation of multiple elements for the Duet date picker webform element.
 *
 * @FormElement("webform_multiple_duet_date")
 */
class WebformMultipleDuetDate extends WebformMultiple {

  /**
   * Build a single element row.
   *
   * @param string $table_id
   *   The element's table id.
   * @param int $row_index
   *   The row index.
   * @param array $element
   *   The element.
   * @param string $default_value
   *   The default value.
   * @param int $weight
   *   The weight.
   * @param array $ajax_settings
   *   An array containing Ajax callback settings.
   *
   * @return array
   *   A render array containing inputs for an element's value and weight.
   */
  protected static function buildElementRow($table_id, $row_index, array $element, $default_value, $weight, array $ajax_settings) {
    $row = parent::buildElementRow($table_id, $row_index, $element, $default_value, $weight, $ajax_settings);
    // Update the row to set the correct row element name and default value.
    $row['_item_']['#name'] = $element['#webform_key'] . '[items][' . $row_index . '][_item_]';
    $row['_item_']['#delta'] = $row_index;
    if (array_key_exists($row_index, $element['#value'])) {
      $row['_item_']['#default_value'] = $element['#value'][$row_index];
    }
    return $row;
  }

}
