<?php

namespace Drupal\duet_date_picker\Plugin\WebformElement;

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElement\Date;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides a 'Duet date picker' element.
 *
 * @WebformElement(
 *   id = "duet_date",
 *   api = "https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Render!Element!Date.php/class/Date",
 *   label = @Translation("Duet Date Picker"),
 *   description = @Translation("Provides a Duet date picker form element for date selection."),
 *   category = @Translation("Date/time elements"),
 * )
 */
class DuetDate extends Date {

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    // Prepare the element using the parent class.
    parent::prepare($element, $webform_submission);
    // Add the Duet date picker library.
    $element['#attached'] = [
      'library' => [
        'duet_date_picker/duet-date-picker',
      ],
    ];
    $date_picker_element_name = 'value';
    $webform_submission_data = $webform_submission->getData();
    if (!empty($webform_submission_data['#webform_key'])) {
      $element['#default_value'] = $webform_submission_data['#webform_key'];
    }
    // Set the element #name value so the html input gets the correct name.
    $element['#name'] = $element['#webform_key'];
    // Theme the widget as a Duet date picker.
    $element['#theme'] = 'duet_date_picker';
    // Set the default value if it exists in the webform submission.
    if (!empty($element['#default_value'])) {
      // Set the default value on the date picker element, that way the
      // value can be correctly rendered by the template.
      if (!is_array($element['#default_value'])) {
        $element['#default_value'] = static::formatDate($element['#date_date_format'], strtotime($element['#default_value']));
      }
      else {
        $element['#default_value'] = array_map(
          fn($item) =>
            static::formatDate($element['#date_date_format'], strtotime($item))
          ,
          $element['#default_value']
        );
      }
    }
    // Set additional Duet date picker widget properties.
    if (!empty($element['#date_date_min'])) {
      $element['#min_date'] = static::formatDate($element['#date_date_format'], strtotime($element['#date_date_min']));
    }
    if (!empty($element['#date_date_max'])) {
      $element['#max_date'] = static::formatDate($element['#date_date_format'], strtotime($element['#date_date_max']));
    }
  }

  /**
   * Webform element validation handler for date elements.
   *
   * Note that #required is validated by _form_validate() already.
   *
   * @see \Drupal\Core\Render\Element\Number::validateNumber
   */
  public static function validateDate(&$element, FormStateInterface $form_state, &$complete_form) {
    // Get the and prepare the correct date value from the duet widget.
    $duet_date_value = $form_state->getUserInput()[$element['#webform_key']];
    if (!is_array($duet_date_value)) {
      $element['#value'] = $duet_date_value;
    }
    else {
      $values = array_column($duet_date_value['items'], '_item_');
      $element['#value'] = $values[$element['#delta']];
    }
    parent::validateDate($element, $form_state, $complete_form);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Hide/unset the "use date picker" as, well, we are a date picker.
    unset($form['date']['datepicker']);
    unset($form['date']['datepicker_button']);
    // Also unset the format form element since we're locked-in to what Duet expects.
    unset($form['date']['date_date_format']);
    return $form;
  }

  /**
   * In this case, we are not the default jquery date picker, but we sure are a date picker.
   */
  protected function datePickerExists() {
    return TRUE;
  }

  /**
   * Force the type to date only.
   */
  protected function getDateType(array $element) {
    return 'date';
  }

  /**
   * Set multiple element wrapper.
   *
   * Override from base class in order to set data required for Duet date picker.
   *
   * @param array $element
   *   An element.
   */
  protected function prepareMultipleWrapper(array &$element) {
    parent::prepareMultipleWrapper($element);
    if (!$this->hasMultipleValues($element) || !$this->hasMultipleWrapper() || empty($element['#multiple'])) {
      return;
    }
    // Override the element's type to use our custom Duet date picker webform multiple element.
    $element['#type'] = 'webform_multiple_duet_date';
    // Unset the top-level element theme and name (only needed on individual rows).
    unset($element['#theme']);
    unset($element['#name']);
  }

}
