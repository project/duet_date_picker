<?php

namespace Drupal\duet_date_picker\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\Date;

/**
 * Extend the default date filter to allow option to add a Duet date picker.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("duet_date")
 */
class DuetDate extends Date {

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['duet'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use Duet date picker (only allows date, not time selection'),
      '#default_value' => $this->options['duet'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['duet'] = [
      'default' => FALSE,
    ];

    return $options;
  }

  /**
   * Add the Duet date picker to the input.
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);
    // Attach Duet Date Picker library.
    if ($this->options['duet']) {
      $form['#attached'] = [
        'library' => [
          'duet_date_picker/duet-date-picker',
        ],
      ];
      // Get the default value (if it exists).
      $default_value = NULL;
      $user_input = $form_state->getUserInput();
      // The exposed filter may be using a custom identifier.
      if (!empty($this->options['expose']['identifier'])) {
        $field_ident = $this->options['expose']['identifier'];
      }
      else {
        $field_ident = $this->field;
      }
      if (!empty($user_input[$field_ident])) {
        $default_value = $user_input[$field_ident];
      }
      if (!empty($form['value']['value'])) {
        $form['value']['value']['#theme'] = 'duet_date_picker';
        $form['value']['value']['#name'] = $field_ident;
        if (!empty($default_value)) {
          $form['value']['value']['#default_value'] = $default_value;
        }
      }
      elseif (!empty($form['value'])) {
        $form['value']['#theme'] = 'duet_date_picker';
        $form['value']['#name'] = $field_ident;
        if (!empty($default_value)) {
          $form['value']['#default_value'] = $default_value;
        }
      }
    }
  }

}
