# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0-beta.0](https://git.drupal.org/project/duet_date_picker/compare/1.0.0-alpha5...1.0.0-beta.0) (2023-08-15)


### Bug Fixes

* **compat:** remove drupal 8 ([522d035](https://git.drupal.org/project/duet_date_picker/commit/522d035a3f4e294bf53b928b4afd1846f572f010))
* **compat:** remove drupal 8 ([4a8cbe9](https://git.drupal.org/project/duet_date_picker/commit/4a8cbe9a492bff63e65815824a3a88bb14c44431))

## [1.0.0-alpha.5](https://git.drupal.org/project/duet_date_picker/compare/1.0.0-alpha4...1.0.0-alpha.5) (2023-06-05)

Add support for Views filters.

## [1.0.0-alpha.4](https://git.drupal.org/project/duet_date_picker/compare/1.0.0-alpha3...1.0.0-alpha.4) (2023-04-12)


### Bug Fixes

* **warnings:** use isset instead of empty ([19bc14a](https://git.drupal.org/project/duet_date_picker/commit/19bc14ab97ee893d6e20840a39e7a4bb34012727))

## [1.0.0-alpha.3](https://git.drupal.org/project/duet_date_picker/compare/1.0.0-alpha2...1.0.0-alpha.3) (2023-01-09)


### Bug Fixes

* **library:** add css categories 1f7b533

## [1.0.0-alpha.2](https://git.drupal.org/project/duet_date_picker/compare/1.0.0-alpha1...1.0.0-alpha.2) (2023-01-09)


### Features

* **ci:** add git.dropfort ci 55814fe


### Bug Fixes

* **composer:** remove copy paste errors 484749d
* **dev:** enable hooks cbf5c51
