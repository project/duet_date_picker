<?php

/**
 * @file
 * Views integration for the Duet date picker module.
 */

use Drupal\duet_date_picker\Plugin\views\filter\DuetDate;
use Drupal\duet_date_picker\Plugin\views\filter\DuetDateTime;

/**
 * Implements hook_views_plugins_filter_alter().
 *
 * Use our custom Duet date views filter.
 * We replace all existing date/datetime filters. The custom Duet filter
 * just extends date/datetime and adds an option to use Duet.
 *
 * Since the Duet date picker only allows date (not time) selection, it can
 * be useful when used in conjunction with the core patch that adds an
 * option for selecting time granularity for views datetime filters.
 *
 * @see https://www.drupal.org/project/drupal/issues/2868014
 */
function duet_date_picker_views_plugins_filter_alter(array &$plugins) {
  // Use the module filter class for both date and datetime filters.
  if (array_key_exists('date', $plugins)) {
    $plugins['date']['class'] = DuetDate::class;
  }
  if (array_key_exists('datetime', $plugins)) {
    $plugins['datetime']['class'] = DuetDateTime::class;
  }
}
