// This file is managed by dropfort/dropfort_module_build.
// Modifications to this file will be overwritten by default.

/** @type {import('stylelint').Config} */
const config = {
  root: true,
  extends: "stylelint-config-coldfront",
};

export default config;
